class Features30 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {
      this.row = document.querySelector('.features-30__accordion-row');
    }

    connectedCallback () {
        this.initFeatures30()
    }

    initFeatures30 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features 30")
        var that = this;
        this.row.addEventListener('click', e => {
          let parent = e.target.closest('.features-30__accordion-row');
          if(parent.classList.contains('features-30__accordion-row--active')) {
            document.querySelector('.features-30__accordion-row--active').classList.remove('features-30__accordion-row--active');
            return;
          }
          if(document.querySelector('.features-30__accordion-row--active')) {
            document.querySelector('.features-30__accordion-row--active').classList.remove('features-30__accordion-row--active');
          }
          parent.classList.add('features-30__accordion-row--active');
      })
    }

}

window.customElements.define('fir-features-30', Features30, { extends: 'div' })
