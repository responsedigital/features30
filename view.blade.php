<!-- Start Features 30 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Two column sectiom with a title, text, accordion block and an image block. -->
@endif
<div class="features-30 {{ $classes }}"  is="fir-features-30">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="features-30__wrap">
    <div class="features-30__copy">
      <h2 class="features-30__title">
        {{ $title ?? 'Lorem ipsum dolor sit amet.' }}
      </h2>
      <div class="features-30__accordion">
        @foreach($accordion as $a)
        <div class="features-30__accordion-row">
          <h6 class="features-30__accordion-title">
            {{ $a['title'] }}
            <svg width="10px" height="10px" viewBox="0 0 10 10" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Features_30_1440" transform="translate(-605.000000, -451.000000)" stroke="#0076FF" stroke-width="1.6">
                        <g id="Group" transform="translate(604.500000, 450.500000)">
                            <line class="minus" x1="5.5" y1="0.5" x2="5.5" y2="10.5"></line>
                            <line x1="5.5" y1="0.5" x2="5.5" y2="10.5" transform="translate(5.500000, 5.500000) rotate(90.000000) translate(-5.500000, -5.500000) "></line>
                        </g>
                    </g>
                </g>
            </svg>
          </h6>
          <p class="features-30__accordion-text">{{ $a['text'] }}</p>
        </div>
        @endforeach
      </div>
    </div>
    <img class="features-30__image" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
  </div>
</div>
<!-- End Features 30 -->
