<?php

namespace Fir\Pinecones\Features30;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Features30',
            'label' => 'Pinecone: Features 30',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Two column sectiom with a title, text, accordion block and an image block."
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'required' => 1
                ],
                [
                    'label' => 'Accordion',
                    'name' => 'accordion',
                    'type' => 'repeater',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'label' => 'Title',
                            'name' => 'title',
                            'type' => 'text'
                        ],
                        [
                            'label' => 'Text',
                            'name' => 'text',
                            'type' => 'textarea'
                        ]
                    ]
                ],
                [
                    'label' => 'Images',
                    'name' => 'imageTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image',
                    'required' => 1
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'flipHorizontal',
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
